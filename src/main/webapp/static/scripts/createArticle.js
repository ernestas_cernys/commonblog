document.addEventListener('DOMContentLoaded', () => {
  const dateInput = document.getElementById('date')
  const today = new Date();
  const date = today.getDate()+'/'+(today.getMonth()+1)+'/'+today.getFullYear();

  dateInput.setAttribute('value', date)
})
