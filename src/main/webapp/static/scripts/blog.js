document.addEventListener('DOMContentLoaded', () => {
  const previous = document.querySelector("#previous")
  const next = document.querySelector("#next")
  const page = document.getElementById('pageNumber')
  const totalPages = document.getElementById('totalPages')
  const previousLink = previous.querySelector('a')
  const nextLink = next.querySelector('a')

  function checkPrevious() {
    if (page.innerText === "1") {
      previous.classList.add('disabled')
      previousLink.classList.add('text-muted')
      previousLink.classList.remove('custom-color')
    } else {
      previous.classList.remove('disabled')
      previousLink.classList.remove('text-muted')
      previousLink.classList.add('custom-color')
    }
  }

  function checkNext() {
    if (page.innerText === totalPages.innerText) {
      next.classList.add('disabled' )
      nextLink.classList.add('text-muted')
      nextLink.classList.remove('custom-color')
    } else {
      next.classList.remove('disabled')
      nextLink.classList.remove('text-muted')
      nextLink.classList.add('custom-color')
    }
  }

  checkNext()
  checkPrevious()


})
