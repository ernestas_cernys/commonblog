<%@ page
  info="Display address weather and the eventual result"
  contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%-- head --%>
<c:import url="/WEB-INF/jsp/_head.jsp"/>

<body>

<!-- Navigation -->
<c:import url="/WEB-INF/jsp/_nav.jsp"/>

<!-- Page Header -->
<header class="masthead" style="background-image: url('<c:url value="/static/images/home-bg.jpg"/>')">
  <div class="overlay"></div>
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <div class="site-heading">
          <h1>A&C dev Blog</h1>
          <span class="subheading">Identification</span>
        </div>
      </div>
    </div>
  </div>
</header>

<!-- Main Content -->
<div class="container">
  <div class="row">
    <div class="col-lg-8 col-md-10 mx-auto">

      <form action="<c:url value="/saveUser"/>" method="POST">

        <div class="d-flex flex-column flex-grow-1">
          <p class="text-success">${success}</p>
          <p class="text-danger">${error}</p>
          <%--   User creation success message   --%>
          <c:choose>
            <c:when test="${success}">

            </c:when>
            <c:when test="${error}">

            </c:when>
            <c:otherwise>
            </c:otherwise>
          </c:choose>

            <p>${username}</p>
          <div class="form-group row flex-grow-1 pb-3 mx-3">
            <label for="username" class=" px-3">Nom d'utilisateur</label>
            <input type="text" name="username" class=" form-control flex-grow-1"
                   id="username" required>
          </div>

          <div class="form-group row flex-grow-1 pb-3 mx-3">
            <label for="password" class="px-3">Mot de passe</label>
            <input type="password" class="form-control flex-grow-1"
                   id="password" name="password" required></input>
          </div>

            <div class="form-group row flex-grow-1 pb-3 mx-3">
              <label for="password" class="px-3">Confirmation de mot de passe</label>
              <input type="password" class="form-control flex-grow-1"
                     id="passwordConfirmation" required></input>
            </div>

        </div>

        <div class="clearfix">
          <div>
            <button class="btn btn-primary" type="submit"/>
            Créer un compte
            </button>
          </div>
        </div>

      </form>

    </div>
  </div>
</div>

<hr>

<!-- Footer -->
<c:import url="/WEB-INF/jsp/_footer.jsp"/>


<!-- Bootstrap core JavaScript -->
<script src="<c:url value="static/scripts/template/jquery/jquery.min.js"/>"></script>
<script src="<c:url value="static/scripts/template/bootstrap/js/bootstrap.bundle.min.js"/>"></script>

<!-- Custom scripts for this template -->
<script src="<c:url value="static/scripts/clean-blog.min.js"/>"></script>
<script src="<c:url value="static/scripts/registration.js"/>"></script>


</body>

</html>
