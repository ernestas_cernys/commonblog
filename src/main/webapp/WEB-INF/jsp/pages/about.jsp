<%@ page
  info="Display address weather and the eventual result"
  contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%-- head --%>
<c:import url="/WEB-INF/jsp/_head.jsp"/>

<body>

<!-- Navigation -->
<c:import url="/WEB-INF/jsp/_nav.jsp"/>

<!-- Page Header -->
<header class="masthead" style="background-image: url('<c:url value="/static/images//about-bg.jpg"/>')">
  <div class="overlay"></div>
  <div class="container">
    <div class="row">
      <div class="col-lg-10 col-md-10 mx-auto">
        <div class="page-heading">
          <h1>À propos de l'équipe</h1>
          <span class="subheading">Akin & Cernys dev</span>
        </div>
      </div>
    </div>
  </div>
</header>

<!-- Main Content -->
<div class="container">
  <div class="row">
    <div class="col-lg-8 col-md-10 mx-auto">
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe nostrum ullam eveniet pariatur voluptates odit, fuga atque ea nobis sit soluta odio, adipisci quas excepturi maxime quae totam ducimus consectetur?</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius praesentium recusandae illo eaque architecto error, repellendus iusto reprehenderit, doloribus, minus sunt. Numquam at quae voluptatum in officia voluptas voluptatibus, minus!</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut consequuntur magnam, excepturi aliquid ex itaque esse est vero natus quae optio aperiam soluta voluptatibus corporis atque iste neque sit tempora!</p>
    </div>
  </div>
</div>

<hr>

<!-- Footer -->
<c:import url="/WEB-INF/jsp/_footer.jsp"/>

<!-- Bootstrap core JavaScript -->
<script src="<c:url value="static/scripts/template/jquery/jquery.min.js"/>"></script>
<script src="<c:url value="static/scripts/template/bootstrap/js/bootstrap.bundle.min.js"/>"></script>

<!-- Custom scripts for this template -->
<script src="<c:url value="static/scripts/clean-blog.min.js"/>"></script>

</body>

</html>
