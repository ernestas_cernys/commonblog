<%@ page
  info="Display address weather and the eventual result"
  contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%-- head --%>
<c:import url="/WEB-INF/jsp/_head.jsp"/>

<body>

<!-- Navigation -->
<c:import url="/WEB-INF/jsp/_nav.jsp"/>

<!-- Page Header -->
<header class="masthead" style="background-image: url('<c:url value="/static/images/home-bg.jpg"/>')">
  <div class="overlay"></div>
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <div class="site-heading">
          <h1>A&C dev Blog</h1>
          <span class="subheading">Ici naisse les Articles extraordinaires</span>
        </div>
      </div>
    </div>
  </div>
</header>

<!-- Main Content -->
<div class="container">
  <div class="row">
    <div class="col-lg-8 col-md-10 mx-auto">

      <form action="<c:url value="/saveArticle"/>" method="post">

        <div class="d-flex flex-column flex-grow-1">
          <div class="form-group row flex-grow-1 pb-3 mx-3">
            <label for="title" class=" px-3">Titre d'article</label>
            <input type="text" name="title" class=" form-control flex-grow-1"
                   id="title">
          </div>

          <div class="form-group row flex-grow-1 pb-3 mx-3">
            <label for="body" class="px-3">Article</label>
            <textarea class="form-control flex-grow-1" name="body"
                      id="body" rows="10"></textarea>
          </div>
        </div>

        <input type="text" name="date" id="date" hidden>

      </form>
      <!-- create -->
      <div class="clearfix d-flex justify-content-center">
        <a class="btn btn-primary w-50" type="submit"/>Créer &rarr;</a>
      </div>
    </div>
  </div>
</div>

<hr>

<!-- Footer -->
<c:import url="/WEB-INF/jsp/_footer.jsp"/>


<!-- Bootstrap core JavaScript -->
<script src="<c:url value="static/scripts/template/jquery/jquery.min.js"/>"></script>
<script src="<c:url value="static/scripts/template/bootstrap/js/bootstrap.bundle.min.js"/>"></script>

<!-- Custom scripts for this template -->
<script src="<c:url value="static/scripts/clean-blog.min.js"/>"></script>
<script src="<c:url value="static/scripts/createArticle.js"/>"></script>

</body>

</html>
