<%@ page
  info="Display address weather and the eventual result"
  contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%-- head --%>
<c:import url="/WEB-INF/jsp/_head.jsp"/>

<body>

<!-- Navigation -->
<c:import url="/WEB-INF/jsp/_nav.jsp"/>

<!-- Page Header -->
<header class="masthead" style="background-image: url('<c:url value="/static/images/home-bg.jpg"/>')">
  <div class="overlay"></div>
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <div class="site-heading">
          <h1>A&C dev Blog</h1>
          <span class="subheading">${title}</span>
        </div>
      </div>
    </div>
  </div>
</header>

<!-- Main Content -->
<div class="container">
  <div class="row">
    <div class="col-lg-8 col-md-10 mx-auto">

      <c:forEach items="${articles}" var="item">
        <div class="post-preview">
            <%-- <a href="post.html">  link to article page --%>
          <a href="">
            <h2 class="post-title">${item.title}</h2>
            <h3 class="post-subtitle">${item.body}</h3>
          </a>
          <p class="post-meta">Posté par <strong>${item.author.userName}</strong> le ${item.date}</p>
        </div>
        <hr>
      </c:forEach>

      <!-- to Blog or next page -->
      <div class="clearfix">
        <a class="btn btn-primary float-right" href="<c:url value="/blog"/>">Voir plus &rarr;</a>
      </div>
    </div>
  </div>
</div>

<hr>

<!-- Footer -->
<c:import url="/WEB-INF/jsp/_footer.jsp"/>


<!-- Bootstrap core JavaScript -->
<script src="<c:url value="static/scripts/template/jquery/jquery.min.js"/>"></script>
<script src="<c:url value="static/scripts/template/bootstrap/js/bootstrap.bundle.min.js"/>"></script>

<!-- Custom scripts for this template -->
<script src="<c:url value="static/scripts/clean-blog.min.js"/>"></script>

</body>

</html>
