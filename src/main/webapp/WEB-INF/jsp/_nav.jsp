<%@ page
  info="Display address weather and the eventual result"
  contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
  <div class="container">
    <a class="navbar-brand" href="<c:url value="/"/>">A&C dev Blog</a>
    <button class=" navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
    data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle
    navigation">
    Menu
    <i class="fas fa-bars"></i>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">

          <a class="nav-link" href="<c:url value="/"/>">Acceuil</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<c:url value="/about"/>">À propos de nous</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<c:url value="/blog"/>">Blog</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<c:url value="/contact"/>">Contact</a>
        </li>
      </ul>
    </div>
  </div>
</nav>
