<%@ page
  info="Display address weather and the eventual result"
  contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="fr">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Learning project Blog">
  <meta name="author" content="Akin&Cernys dev">

  <title><c:out value="${title}"></c:out></title>

  <!-- Bootstrap core CSS -->
  <link href="<c:url value="/static/styles/template/bootstrap.min.css"/>" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="<c:url value="/static/styles/template/all.min.css"/>" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet'
        type='text/css'>
  <link
    href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
    rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="<c:url value="/static/styles/clean-blog.min.css"/>" rel="stylesheet">

  <%--  favicon--%>
  <link rel="apple-touch-icon" sizes="180x180" href="<c:url value="/static/images/favicon/apple-touch-icon.png"/>">
  <link rel="icon" type="image/png" sizes="32x32" href="<c:url value="/static/images/favicon/favicon-32x32.png"/>">
  <link rel="icon" type="image/png" sizes="16x16" href="<c:url value="/static/images/favicon/favicon-16x16.png"/>">
  <link rel="manifest" href="<c:url value="/static/images/favicon/site.webmanifest"/>">
</head>
