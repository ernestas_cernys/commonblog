package com.cernys.dao;


import com.cernys.models.Article;
import java.util.List;

public interface DaoArticle {


  List<Article> findArticlesByNumberOfPage (int page, int nbArticle);
  int findNombreArticles();

  boolean createArticle(Article article);


}
