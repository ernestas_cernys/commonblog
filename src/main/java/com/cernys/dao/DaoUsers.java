package com.cernys.dao;


import com.cernys.models.User;

public interface DaoUsers {

  User findById (long id);

  User findByNameAndPassword (String username, String password);
  boolean save(User user);


  //public void save(<T>,user);


}
