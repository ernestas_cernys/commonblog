package com.cernys.servlets;



import com.cernys.dao.*;
import com.cernys.models.Article;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/")
public class HomeServlet extends HttpServlet {
  private DaoUsers daoUsers;
  private DaoArticle daoArticle;

  @Override
  public void init() throws ServletException{
    DaoFactory daoFactory = DaoFactory.getInstance();
    daoUsers = new DaoUserImplement(daoFactory);
    daoArticle = new DaoArticleImplement(daoFactory);
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    String title = "Notre incroyable Blog :" ;
    req.setAttribute("title", title);


    List<Article> articles = new ArrayList<>();

    articles = daoArticle.findArticlesByNumberOfPage(1,3);

    req.getSession().setAttribute("articles", articles);
    req.getSession().setAttribute("articlesCount", articles.size());

    req.getRequestDispatcher("/WEB-INF/jsp/pages/index.jsp").forward(req, resp);
  }

}
