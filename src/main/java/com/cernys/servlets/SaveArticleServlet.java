package com.cernys.servlets;

import com.cernys.dao.*;
import com.cernys.models.Article;
import com.cernys.models.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/saveArticle")
public class SaveArticleServlet extends HttpServlet {
  private DaoUsers daoUsers;
  private DaoArticle daoArticle;

  @Override
  public void init() throws ServletException{
    DaoFactory daoFactory = DaoFactory.getInstance();
    daoUsers = new DaoUserImplement(daoFactory);
    daoArticle = new DaoArticleImplement(daoFactory);
  }
  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    String title = req.getParameter("title");
    String body = req.getParameter("body");
    User user = (User)req.getSession().getAttribute("user");
    String date = req.getParameter("date");

    Article article = new Article();
    article.setAuthor(user);
    article.setTitle(title);
    article.setBody(body);
    article.setDate(date);

//    if (daoArticle.saveArticle(article)){
//      req.setAttribute("success", "Article sauvgardé");
//    } else {
//      req.setAttribute("error", "Echec de sauvgarde");
//    }
    req.getRequestDispatcher("/WEB-INF/jsp/pages/blog.jsp").forward(req, resp);
  }


}
