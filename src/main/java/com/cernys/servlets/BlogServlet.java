package com.cernys.servlets;

import com.cernys.dao.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/blog")
public class BlogServlet extends HttpServlet {
  private DaoUsers daoUsers;
  private DaoArticle daoArticle;

  @Override
  public void init() throws ServletException{
    DaoFactory daoFactory = DaoFactory.getInstance();
    daoUsers = new DaoUserImplement(daoFactory);
    daoArticle = new DaoArticleImplement(daoFactory);
  }

  private final int ARTICLES_IN_PAGE = 5;

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    int articlesCount = 0;
    int page = 1;
    // transfer to navigator how many articles in page
    req.setAttribute("articlesInPage", ARTICLES_IN_PAGE);

    // transfer to navigator which page is open
    if (req.getParameter("page") == null){
      page = 1;
    } else{
      page = Integer.parseInt(req.getParameter("page"));
    }
    req.setAttribute("page", page);

//    try {
      articlesCount = daoArticle.findNombreArticles();
      req.setAttribute("articles", daoArticle.findArticlesByNumberOfPage(page, ARTICLES_IN_PAGE));
//    }
//    catch (SQLException e) {
//      // faire page d'erreur
//    }

    req.setAttribute("articlesCount", articlesCount);
    req.getRequestDispatcher("/WEB-INF/jsp/pages/blog.jsp").forward(req, resp);
  }

}
