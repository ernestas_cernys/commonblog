drop schema if exists blog_db;
create schema blog_db;
use blog_db; 

drop table if exists ARTICLES;
drop table if exists USERS;

create table USERS (
   ID_USER              bigint                  not null auto_increment,
   USERNAME             VARCHAR(200)            not null,
   USER_PASSWORD        VARCHAR(16000)          not null,
   primary key (ID_USER)
);

create table ARTICLES (
   ID_ARTICLE           bigint                  not null auto_increment,
   ID_USER              bigint                  not null,
   DATE_ARTICLE         date                    not null,
   TITLE_ARTICLE        VARCHAR(500)            not null,
   BODY_ARTICLE         TEXT                    not null,

   primary key (ID_ARTICLE),
   foreign key (ID_USER) references USERS(ID_USER)
);


-- Requette type pour récupérer les articles --

/*
SELECT ID_ARTICLE, ID_USER, DATE_ARTICLE, TITLE_ARTICLE, BODY_ARTICLE 
FROM blog_db.articles ORDER BY DATE_ARTICLE DESC LIMIT 5 ;
*/

SELECT ID_USER, USERNAME FROM USERS WHERE ID_USER=1;

